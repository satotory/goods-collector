<?php

namespace App\modules\Korzilla\Collector\Service;

use App\modules\Korzilla\Collector\Inputs\GetAllProductsInput;
use App\modules\Korzilla\Collector\Inputs\GetManyProductsInput;
use App\modules\Korzilla\Collector\Repositories\ProductRepository;
use App\modules\Korzilla\Collector\Tasks\GetAllProductsTask;
use App\modules\Korzilla\Collector\Tasks\GetManyProductsTask;
use App\modules\Korzilla\YML\Avito\Model\DB;

class CollectorService
{
    /**
     * @var DB
     */
    private $model;

    /**
     * @param DB $model
     */
    public function __construct(DB $model)
    {
        $this->$model = $model;
    }

    /**
     * @param int $category_id
     * @return \App\modules\Korzilla\Collector\Entities\Product[]
     */
    public function selectProductsByCategoryID(GetManyProductsInput $input)
    {
        return (new GetManyProductsTask(new ProductRepository($this->model)))
            ->run($input);
    }

    public function selectAllProducts(GetAllProductsInput $input)
    {
        return (new GetAllProductsTask(new ProductRepository($this->model)))
            ->run($input);
    }
}