<?php

namespace App\modules\Korzilla\Collector\Entities;

class Product
{
    /**
     * Message_ID
     * @var int
     */
    public $id;
    
    /**
     * Subdivision_ID
     * @var int
     */
    public $category_id;

    /**
     * name
     * @var string
     */
    public $name;

    /**
     * text
     * @var string
     */
    public $description;
}