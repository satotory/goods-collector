<?php

namespace App\modules\Korzilla\Collector\Tasks;

use App\modules\Korzilla\Collector\Inputs\GetAllProductsInput;
use App\modules\Korzilla\Collector\Repositories\ProductRepository;

class GetAllProductsTask
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetAllProductsInput $input
     * @return \App\modules\Korzilla\Collector\Entities\Product[]
     */
    public function run(GetAllProductsInput $input)
    {
        $offset = $input->getOffset();
        $rowcount = $input->getRowcount();

        return $this->repository->selectAllProducts($offset, $rowcount);
    }
}