<?php

namespace App\modules\Korzilla\Collector\Tasks;

use App\modules\Korzilla\Collector\Inputs\GetManyProductsInput;
use App\modules\Korzilla\Collector\Repositories\ProductRepository;

class GetManyProductsTask
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetManyProductsInput $input
     * @return \App\modules\Korzilla\Collector\Entities\Product[]
     */
    public function run(GetManyProductsInput $input)
    {
        return $this->repository->findManyByCategoryID($input->getCriteria());
    }
}