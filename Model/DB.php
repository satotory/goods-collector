<?php

namespace App\modules\Korzilla\Collector\Model;

use \nc_db;

class DB
{
    /**
     * @var nc_db
     */
    private $nc_db;

    public function __construct(nc_db $nc_db)
    {
        $this->nc_db = $nc_db;
    }

    /**
     * @param $query SQL query
     * @param $offset Смещение, с которой должна происходить выборка
     * @param $rowcount Количество извлекаемых строк
     * @param $output ARRAY_A, OBJECT
     */
    public function get_results($query, $offset = "0", $rowcount = "24", $output = ARRAY_A)
    {
        $sql = $query . " LIMIT " . ($offset ? "{$offset}," : "") . $rowcount;

        return $this->nc_db->get_results($sql, $output);
    }
}