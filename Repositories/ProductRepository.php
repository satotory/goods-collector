<?php

namespace App\modules\Korzilla\Collector\Repositories;

use App\modules\Korzilla\Collector\Entities\Product;
use App\modules\Korzilla\YML\Avito\Model\DB;

class ProductRepository
{
    /**
     * @var string
     */
    private $tableName = "Message2001";

    /**
     * Префикс таблицы
     * @var string
     */
    private $prefix = "m";

    /**
     * @var DB
     */
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }
    
    /**
     * @param int $subdivision_id Ид категории
     * @return Product[]
     */
    public function findManyByCategoryID(int $category_id)
    {
        $result = [];

        $prefix = $this->prefix;

        $fields = "{$prefix}.*";

        $sql = "SELECT {$fields} 
                        FROM `{$this->tableName}` as {$prefix}
                        WHERE {$prefix}.`Subdivision_ID` = {$category_id}";
        
        
        foreach ($this->model->get_results($sql) ?: [] as $product) {
            $p = new Product();
            $p->name = $product["name"];
            $p->description = $product["text"];
            $p->id = $product["Message_ID"];
            $p->category_id = $product["Subdivision_ID"];

            $result[] = $p;
        }

        return $result;
    }

    public function selectAllProducts($offset = "0", $rowcount = "24")
    {
        $result = [];

        $prefix = $this->prefix;

        $fields = "{$prefix}.*";

        $sql = "SELECT {$fields} 
                        FROM `{$this->tableName}` as {$prefix}
                        LIMIT {$offset}, {$rowcount}";
        
        
        foreach ($this->model->get_results($sql) ?: [] as $product) {
            $p = new Product();
            $p->name = $product["name"];
            $p->description = $product["text"];
            $p->id = $product["Message_ID"];
            $p->category_id = $product["Subdivision_ID"];

            $result[] = $p;
        }

        return $result;
    }
}