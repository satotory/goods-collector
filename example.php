<?php
use App\modules\Korzilla\Collector\Inputs\GetAllProductsInput;
use App\modules\Korzilla\Collector\Model\DB;
use App\modules\Korzilla\Collector\Service\CollectorService;

$db = (\nc_Core::get_object())->db;

$model = new DB($db);

$service = new CollectorService($model);


$offset = 0;
$rowcount = 24;

while ($stop == false) {

    $input = new GetAllProductsInput($offset, $rowcount);

    $products = $service->selectAllProducts($input);

    if (!$result)  {
        $stop = true;
    }

    $result = array_merge($result, $products);

    $offset += 24;
    $rowcount += 24;
}