<?php

namespace App\modules\Korzilla\Collector\Inputs;

class GetAllProductsInput
{
    /**
     * Смещение, с которой должна происходить выборка
     * @var int
     */
    private $offset;
    
    /**
     * Количество извлекаемых строк
     * @var int
     */
    private $rowcount;

    /**
     * @param int $offset Количество извлекаемых строк
     * @param int $rowcount Смещение, с которой должна происходить выборка
     */
    public function __construct(int $offset, int $rowcount)
    {
        $this->offset = $offset;
        $this->rowcount = $rowcount;
    }

	/**
	 * Количество извлекаемых строк
	 * @return int
	 */
	public function getRowcount() 
    {
		return $this->rowcount;
	}

	/**
	 * Смещение, с которой должна происходить выборка
	 * @return int
	 */
	public function getOffset() 
    {
		return $this->offset;
	}
}