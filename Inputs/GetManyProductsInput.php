<?php

namespace App\modules\Korzilla\Collector\Inputs;

class GetManyProductsInput
{
    /**
     * Category_ID
     * @var int
     */
    private $criteria;

    /**
     * @param $criteria ID раздела
     */
    public function __construct(int $criteria)
    {
        $this->criteria = $criteria;
    }

	/**
	 * Category_ID
	 * @return int
	 */
	public function getCriteria() {
		return $this->criteria;
	}
}